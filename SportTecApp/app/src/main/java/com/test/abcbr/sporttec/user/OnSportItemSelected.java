package com.test.abcbr.sporttec.user;

import com.test.abcbr.sporttec.models.Sport;

/**
 * Created by abcbr on 25/11/2017.
 */

public interface OnSportItemSelected {
    void onItemSportCheckedChange(Sport item, boolean isChecked);
}
