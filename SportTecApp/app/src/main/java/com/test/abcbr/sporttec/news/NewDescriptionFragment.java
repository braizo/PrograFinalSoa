package com.test.abcbr.sporttec.news;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.test.abcbr.sporttec.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewDescriptionFragment extends Fragment {

    public static final String TITLE_KEY = "title";
    public static final String CONTENT_KEY = "content";
    public static final String IMAGE_KEY = "image";

    private ImageView img;
    private TextView txtvTitle;
    private TextView txtvContent;

    private String image_url;
    private String title;
    private String content;

    public NewDescriptionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle != null){
            title = bundle.getString(TITLE_KEY);
            content = bundle.getString(CONTENT_KEY);
            image_url = bundle.getString(IMAGE_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_new_description, container, false);

        img = view.findViewById(R.id.new_description_img);

        txtvTitle = view.findViewById(R.id.new_description_title);
        txtvContent = view.findViewById(R.id.new_description_content);

        Ion.with(img).placeholder(R.drawable.ic_menu_news)
                .error(R.drawable.ic_image_error).load(image_url);

        txtvTitle.setText(title);

        txtvContent.setText(content);

        return view;
    }

}
