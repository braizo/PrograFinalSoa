package com.test.abcbr.sporttec.user;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.test.abcbr.sporttec.R;
import com.test.abcbr.sporttec.connection.ConnectionManager;
import com.test.abcbr.sporttec.models.User;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mName;
    private EditText mEmail;
    private EditText mPass;
    private EditText mPassConfirm;

    private Button mBtnCreateAccount;

    private TextView mTxtvToLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);

        mName = findViewById(R.id.signup_name);
        mEmail = findViewById(R.id.signup_email);
        mPass = findViewById(R.id.signup_password);
        mPassConfirm = findViewById(R.id.signup_confirm_password);

        mBtnCreateAccount = findViewById(R.id.signup_create_account);
        mTxtvToLogin = findViewById(R.id.signup_to_login);

        mBtnCreateAccount.setOnClickListener(this);
        mTxtvToLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.signup_create_account:
                createAccount(v);
                break;
            case R.id.signup_to_login:
                toLogin();
                break;
            default:
                break;
        }
    }

    private void createAccount(final View v){
        boolean correct = true;

        String name, email, pass, pass_conf = "";

        name = mName.getText().toString();
        email = mEmail.getText().toString();
        pass = mPass.getText().toString();
        pass_conf = mPassConfirm.getText().toString();

        if(name.isEmpty()){
            mName.setError("The name field si required");
            correct = false;
        }

        if(email.isEmpty()){
            mEmail.setError("The email field si required");
            correct = false;
        }

        if(pass.isEmpty()){
            mPass.setError("The password field si required");
            correct = false;
        }
        if(pass.compareTo(pass_conf) != 0){
            mPassConfirm.setError("The passwords don\'t match");
            correct = false;
        }

        if(correct){
            User user = new User();
            user.mName = name;
            user.mEmail = email;
            user.mImgUrl = "image_url";
            user.mPassword = pass;

            JsonObject user_json = user.getJsonPost();

            Ion.with(this)
                    .load("POST", "http://" + ConnectionManager.getIP() + User.USER_PATH)
                    .setJsonObjectBody(user_json)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            Gson gson = new Gson();
                            User user_result = gson.fromJson(result, User.class);
                            toLogin();
                        }
                    });
        }

    }

    private void toLogin(){
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }
}
