package com.test.abcbr.sporttec.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by abcbr on 25/11/2017.
 */

public class New {

    public static final String NEWS_PATH = ":3000/api/News";

    public static final String NEWS_TITLE = "title";
    public static final String NEWS_CONTENT = "content";
    public static final String NEWS_IMAGE_URL = "image_url";
    public static final String NEWS_DATE = "date";
    public static final String NEWS_SPORT = "sport";
    public static final String NEWS_ID = "id";

    @SerializedName(NEWS_TITLE)
    public String mTitle;
    @SerializedName(NEWS_CONTENT)
    public String mContent;
    @SerializedName(NEWS_IMAGE_URL)
    public String mImageUrl;
    @SerializedName(NEWS_DATE)
    public String mDate;
    @SerializedName(NEWS_SPORT)
    public String mSport;
    @SerializedName(NEWS_ID)
    public String mId;

    public New(){}

    public New(String mTitle, String mContent, String mImageUrl, String mDate, String mSport) {
        this.mTitle = mTitle;
        this.mContent = mContent;
        this.mImageUrl = mImageUrl;
        this.mDate = mDate;
        this.mSport = mSport;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmContent() {
        return mContent;
    }

    public void setmContent(String mContent) {
        this.mContent = mContent;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }

    public String getmSport() {
        return mSport;
    }

    public void setmSport(String mSport) {
        this.mSport = mSport;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }
}
