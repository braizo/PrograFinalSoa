package com.test.abcbr.sporttec;

import com.test.abcbr.sporttec.models.New;

/**
 * Created by abcbr on 25/11/2017.
 */

public interface FragmentListener {

    void loadNew(New pNew);
}
