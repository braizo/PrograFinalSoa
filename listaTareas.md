## Server

- Modelos
- Probar con Postman
- Swagger
- Tal vez filtros (si da chance)

___

## App

1. Splash
1. Sing in
1. Log in (sin mucha lógica)
1. Lista de noticias (Pantalla principal)
  - Noticia principal
  - lista de noticias
    - imagen
    - título
    - fecha
1. Perfil de usuario
  - Editar Nombre
  - Editar deportes preferencia
  - Editar foto
1. Pantalla noticia
  - imagen
  - título
  - contenido
1. Pantalla de deportes
  - deportes soportados
  - al tap historial de resultados, teams y retos
1. Historial de resultados
  - Lista de resultados de mi equipo
  - Lista de resultados de los demás
  - respecto a un deporte
1. Perfil de equipo
  - foto del equipo
  - nombre del quipo
  - invitación integrante
  - eliminar integrante
  - posibilidad de crearlo
1. Pantalla de retos
  - retos de mi equipo
  - lista de equipos ordenados mayor cantidad de ganados
  - registrar un reto a ese equipo
